@extends('layouts.app')

@section('content')
    <!--slider-->
        @include('partials.slider')
    <!--/slider-->

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>
                        <!--category-products-->
                            @include('partials.products-categories')
                        <!--/category-products-->

                        <!--brands_products-->
                            @include('partials.products-brands')
                        <!--/brands_products-->

                        <!--price-range-->
                            @include('partials.price-range')
                        <!--/price-range-->

                        <div class="shipping text-center"><<!doctype html>
                            <html lang="en">
                            <head>
                                <meta charset="UTF-8">
                                <title>Document</title>
                            </head>
                            <body>
                            
                            </body>
                            </html>--shipping-->
                            <img src="images/home/shipping.jpg" alt="" />
                        </div><!--/shipping-->

                    </div>
                </div>

                <div class="col-sm-9 padding-right">
                    <!--features_items-->
                        @include('partials.features-items')
                    <!--features_items-->

                    <!--category-tab-->
                        @include('partials.category-tab')
                    <!--/category-tab-->

                    <!--recommended_items-->
                    @include('partials.recommended-items')
                    <!--/recommended_items-->
                </div>
            </div>
        </div>
    </section>

@stop