<header id="header"><!--header-->
   <!--header_top-->
        @include('layouts.header-top')
    <!--/header_top-->

    <!--header-middle-->
          @include('layouts.header-middle')
    <!--/header-middle-->

    <!--header-bottom-->
        @include('layouts.header-bottom')
    <!--/header-bottom-->
</header><!--/header-->
