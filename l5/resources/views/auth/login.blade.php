@extends('layouts.app')

@section('content')
<section id="form"><!--form-->

    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>Login to your account</h2>
                    <form method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <input type="text" placeholder="Email"  name="email" value="{{ old('email') }}"
                               class="{{ $errors->has('email') ? ' has-error' : '' }}" />
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                        <input type="password"  name="password" placeholder="Password"
                               class="{{ $errors->has('password') ? ' has-error' : '' }}"/>
							<span>
								<input type="checkbox" class="checkbox"  name="remember">
								Keep me signed in
							</span>
                            <button type="submit" class="btn btn-primary " >
                                <i class="fa fa-btn fa-sign-in"></i>Login
                            </button>

                            <a href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                    </form>
                </div><!--/login form-->
            </div>
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->
                    <h2>New User Signup!</h2>
                    <form method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                        <input type="text" placeholder="Name" name="name" value="{{ old('name') }}"
                                    class="{{ $errors->has('name') ? ' has-error' : '' }}"/>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <input type="email" placeholder="Email Address" name="email" value="{{ old('email') }}"
                                class="{{ $errors->has('email') ? ' has-error' : '' }}"/>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                        <input type="password" placeholder="Password" name="password"
                               class="{{ $errors->has('password') ? ' has-error' : '' }}"/>

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                        <input type="password" placeholder="Password" name="password_confirmation"
                               class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}"/>

                        <button type="submit" class="btn btn-default">Signup</button>
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->

@endsection


