@extends('layouts.app')

@section('content')

    <div class="container text-center">
        <div class="logo-404">
            <a href="index.html"><img src="/images/home/logo.png" alt=""/></a>
        </div>
        <div class="content-404">
            <img src="/images/404/404.png" class="img-responsive" alt=""/>
            <h1>{{$message}} {{$product_name}}</h1>
            <p>{{$detailed_message}}</p>
            <h2><a href="{{route('eshopper.index')}}">Bring me back Home</a></h2>
        </div>
    </div>
@stop