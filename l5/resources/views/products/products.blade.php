@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('partials.sidebar')
                <div class="col-sm-9">
                    <div class="row">
                        @foreach($products  as $product)
                            <div class="col-sm-3" >
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            @if($product->images()->first())
                                                <img src="{{$product->images()->first()['image_path']}}" alt=""/>
                                            @endif
                                            <h2>${{$product->prices()->first()['value']}}</h2>
                                            <p>{{$product->name}}</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>Add
                                                to cart</a>
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">
                                                <span> <a href="{{route('products.show',$product->id )}}"> Product
                                                        Details</a></span>
                                                <h2>${{$product->prices()->first()['value']}}</h2>
                                                <p>{{$product->name}}</p>
                                                <a href="#" class="btn btn-default add-to-cart"><i
                                                            class="fa fa-shopping-cart" ></i>
                                                    Add to cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">
                                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop