@extends('layouts.app')


@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('partials.sidebar')
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-5">
                        <div class="view-product">
                            <img src="{{$product->images[0]->image_path}}" alt=""/>
                            <h3>ZOOM</h3>
                        </div>
                        <div id="similar-product" class="carousel slide" data-ride="carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php $i = 0 ;  $temp= 0 ?>

                                @for($i= 0 ;  $i < count($product->images); $i++)
                                    @if($i % 3== 0)
                                            <div class="item {{ ($i==0) ? 'active' :''}}">
                                    @endif
                                    <?php $j= $i ;?>

                                    @for($j= $i ;  $j < $i+3;$j++)
                                                @if($j < count($product->images))
                                                 <a href=""><img src="{{$product->images[$j]->image_path}}" alt="" width="90px" ></a>
                                                        <?php $temp=  $j; ?>
                                                @endif
                                    @endfor

                                    @if($i % 3 == 0)
                                        </div>
                                    @endif
                                    <?php $i=  $temp ; ?>
                                    @endfor

                            </div>

                            <!-- Controls -->
                            <a class="left item-control" href="#similar-product" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right item-control" href="#similar-product" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                            <img src="/images/product-details/new.jpg" class="newarrival" alt=""/>
                            <h2>{{$product->name}}</h2>
                            <p>Web ID: {{$product->reference}}</p>
                            <img src="/images/product-details/rating.png" alt=""/>
								<span>
									<span>US ${{$product->prices()->first()->value}}</span>
									<label>Quantity:</label>
                                     <input value="{{$product->stock->quantity}}" readonly>
									<button type="button" class="btn btn-fefault cart">
                                        <i class="fa fa-shopping-cart"></i>
                                        Add to cart
                                    </button>
								</span>
                            <p><b>Availability: {{$product->availability->type}}</b></p>
                            <p><b>Condition:</b> {{$product->condition->value}}</p>
                            <p><b>Brand:</b> E-SHOPPER</p>
                            <a href=""><img src="/images/product-details/share.png" class="share img-responsive"
                                            alt=""/></a>
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->

            </div>
        </div>
    </section>
@stop