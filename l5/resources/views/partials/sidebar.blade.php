<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Category</h2>
        <!--category-products-->
        @include('partials.products-categories')
        <!--/category-products-->

        <!--brands_products-->
        @include('partials.products-brands')
        <!--/brands_products-->

        <!--price-range-->
        @include('partials.price-range')
        <!--/price-range-->

        <div class="shipping text-center"><<!doctype html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>Document</title>
            </head>
            <body>

            </body>
            </html>--shipping-->
            <img src="images/home/shipping.jpg" alt="" />
        </div><!--/shipping-->

    </div>
</div>
