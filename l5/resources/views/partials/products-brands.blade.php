<div class="brands_products">
    <h2>Brands</h2>
    <div class="brands-name">
        <ul class="nav nav-pills nav-stacked">
            @foreach($brands as $brand )
            <li><a href="#"> <span class="pull-right">({{count($brand->products)}})</span>{{$brand->name}}</a></li>
            @endforeach
        </ul>
    </div>
</div>