<div class="panel-group category-products" id="accordian">
    @foreach($categories as $category)

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                    {{$category->name }}
                </a>
            </h4>
        </div>

        <div id="sportswear" class="panel-collapse collapse">
            <div class="panel-body">
                <ul>

                    @foreach($category->getDescendants() as  $sub)
                    <li><a href="{{route('eshopper.category.products', $sub->id)}}" >{{$sub->name}} </a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endforeach

</div>