@if($paginator->getLastPage() > 1)
    <div class="pagination">
        {{ with(new Eshopper\Pagination\EshopperPresenter($paginator))->render() }}
    </div>
@endif