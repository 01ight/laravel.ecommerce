@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('partials.sidebar')
                <div class="col-sm-9">

                    <div class="blog-post-area">
                        <h2 class="title text-center">Latest From our Blog</h2>
                        @foreach($posts as $post )
                            <div class="single-blog-post">
                                <h3>{{$post->title}}</h3>
                                <div class="post-meta">
                                    <ul>
                                        <li><i class="fa fa-user"></i> {{$post->user->name}}</li>
                                        <li><i class="fa fa-clock-o"></i> {{date_format($post->created_at,"H:i")}}</li>
                                        <li><i class="fa fa-calendar"></i> {{date_format($post->created_at,"d/m , y")}}</li>
                                    </ul>
								<span>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
								</span>
                                </div>
                                <a href="">
                                    <img src="{{$post->image_path}}" alt="">
                                </a>
                                <p> {{str_limit($post->body, 100)}}</p>
                                <a class="btn btn-primary" href="{{route('posts.show',$post->id)}}">Read More</a>
                            </div>
                        @endforeach
                        <div class="pagination-area">
                            {{with(new Eshopper\Repositories\EshopperPresenter($posts))->render()}}
                        </div>
{{--                        {{ $posts->links('partials.pagination.default') }}--}}
{{--                        @include('partials.pagination.default', ['paginator', $posts])--}}

                    </div>
                </div>
            </div>
        </div>
    </section>
@stop