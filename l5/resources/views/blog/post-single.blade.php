@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('partials.sidebar')
                <div class="col-sm-9">

                    <div class="blog-post-area">
                        <h2 class="title text-center">Latest From our Blog</h2>
                            <div class="single-blog-post">
                                <h3>{{$post->title}}</h3>
                                <div class="post-meta">
                                    <ul>
                                        <li><i class="fa fa-user"></i> {{$post->user->name}}</li>
                                        <li><i class="fa fa-clock-o"></i> {{date_format($post->created_at,"H:i")}}</li>
                                        <li><i class="fa fa-calendar"></i> {{date_format($post->created_at,"d/m , y")}}</li>
                                    </ul>
								<span>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
								</span>
                                </div>
                                <a href="">
                                    <img src="{{$post->image_path}}" alt="">
                                </a>
                                <p> {{$post->body}}</p>
                                <div class="pager-area">
                                    <ul class="pager pull-right">
                                        @if($previousPost)
                                            <li><a href="{{url('posts/'.$previousPost)}}">Pre</a></li>
                                        @endif
                                        @if($nextPost)
                                            <li><a href="{{url('posts/'.$nextPost)}}">Next</a></li>
                                        @endif

                                    </ul>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop