<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $fillable = [
        'value',
        'product_id',
    ];

    public function  products() {
        return  $this->hasMany('Eshopper\Product') ;
    }
}
