<?php

namespace  Eshopper\Exceptions ;

use Eshopper\Product;

class ProductNotFoundException extends \Exception {

    protected $productName ;
    protected $message = 'OPPS! We Couldn’t Find this product qsdqsjbd' ;
    protected $detailedMessage = 'Uh... So it looks like you brock something. The product you are looking for has up and Vanished.' ;

    /**
     * ProductNotFoundException constructor.
     * @param string $productName
     * @param string $message
     * @param null $detailedMessage
     * @internal param Product $product
     */
    public function  __construct($productName  , $message= null, $detailedMessage = null)
    {
        $this->productName = $productName;
        $this->message = ($message) ? $message : $this->message;
        $this->detailedMessage = ($detailedMessage) ? $detailedMessage : $this->detailedMessage;
    }

    /**
     * @return string
     */
    public function getExceptionMessage() {
        return  $this->message ;
    }

    public function getExceptionDetailedMessage() {
        return  $this->message ;
    }
    public  function  getProductName() {
        return  $this->productName;
    }



}