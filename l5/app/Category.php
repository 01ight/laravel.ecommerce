<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\Node;

class Category extends Node
{

    protected $fillable = [
        'name',
        'description'
    ];

    public function  products(){
        return  $this->hasMany('Eshopper\Product') ;
    }

}
