<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});

Route::get('Contact' , [
    'as' => 'Eshopper.contact',
    'uses'=>'StaticController@contact'
]) ;

Route::get('/index', [
        'as' => 'eshopper.index',
        'uses' => 'IndexController@index'
    ]
);
// product routes
Route::resource('products',  'ProductController') ;

// products of  categories routes

Route::get('category/{catId}/products', [
    'as' => 'eshopper.category.products' ,
    'uses'=> 'CategoryController@productsOfCategoryAndSub'
]);

// blog rotues

Route::resource('posts' , 'PostController') ;

// Cart routes

Route::get('cart', [
    'as' => 'Eshopper.cart' ,
    'uses' =>'CartController@show'
]) ;