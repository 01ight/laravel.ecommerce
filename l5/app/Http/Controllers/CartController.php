<?php

namespace Eshopper\Http\Controllers;

use Illuminate\Http\Request;

use Eshopper\Http\Requests;
use Eshopper\Http\Controllers\Controller;

class CartController extends Controller
{

    public function show(Request $request  )
    {
        $carts = $request->session()->get('products');
        return  view('cart.cart')->with('carts' , $carts) ;
    }

    public function  save(Request $request , $productId) {

        if($request->session()->has('products'))

            $request->session()->push('products', $productId) ;

        else $request->session()->push('products', $productId) ;
    }
    public function  store () {

    }
}
