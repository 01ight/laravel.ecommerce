<?php

namespace Eshopper\Http\Controllers;

use Eshopper\Repositories\BrandRepository;
use Eshopper\Repositories\CategoryRepository;
use Eshopper\Repositories\ProductRepository;
use Illuminate\Http\Request;

use Eshopper\Http\Requests;
use Eshopper\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    protected  $productRepository   ;
    protected  $brandRepository ;
    protected  $categoryRepository;

    public function  __construct( ProductRepository $productRepository, BrandRepository $brandRepository,  CategoryRepository $categoryRepository){

        $this->productRepository = $productRepository ;
        $this->brandRepository = $brandRepository ;
        $this->categoryRepository = $categoryRepository ;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productRepository->getPaginatedProducts(10) ;

        return  view('products.products')->with('products' , $products) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->productRepository->getAllProductInfo($id) ;

        return view('products.product-details')->with('product', $product) ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
