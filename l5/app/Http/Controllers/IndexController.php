<?php

namespace Eshopper\Http\Controllers;

use Illuminate\Http\Request;

use Eshopper\Http\Requests;
use Eshopper\Http\Controllers\Controller;

class IndexController extends Controller
{


    public function  index() {

        return view('index') ;
    }
}
