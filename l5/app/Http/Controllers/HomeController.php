<?php

namespace Eshopper\Http\Controllers;

use Eshopper\Http\Requests;
use Eshopper\Http\Controllers\Controller;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.

     * @return Response
     */
    public function index()
    {
        return view('home');
    }

}
