<?php

namespace Eshopper\Http\Controllers;

use Eshopper\Repositories\ProductRepository;
use Illuminate\Http\Request;

use Eshopper\Http\Requests;
use Eshopper\Http\Controllers\Controller;

class CategoryController extends Controller
{

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param $catId
     * @return \Illuminate\Http\JsonResponse
     */
    public function productsOfCategoryAndSub($catId)
    {
        $products  = $this->productRepository->getProductsOfCategory($catId);
        return view('products.products-by-category')->with('products' , $products) ;
    }

}
