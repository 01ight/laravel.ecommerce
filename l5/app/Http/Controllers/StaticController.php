<?php

namespace Eshopper\Http\Controllers;

use Illuminate\Http\Request;

use Eshopper\Http\Requests;
use Eshopper\Http\Controllers\Controller;

class StaticController extends Controller
{
    public function  contact() {
        return  view('contact') ;
    }
}
