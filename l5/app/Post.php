<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected  $fillable = [
        'title',
        'body' ,
        'user_id',
        'image_path',
    ] ;

    public function  user(){
        return $this->belongsTo('Eshopper\User') ;
    }
}
