<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
            'name' ,
            'description' ,
            'reference' ,
            'rating' ,
            'details',
    ] ;


    public function  prices () {
        return $this->hasMany('Eshopper\Price') ;
    }

    public function  stock () {
        return $this->belongsTo('Eshopper\Stock') ;
    }

    public function  availability() {
        return $this->belongsTo('Eshopper\Availability') ;
    }

    public function  condition() {
        return $this->belongsTo('Eshopper\Condition') ;
    }
    public function  state() {
        return $this->belongsTo('Eshopper\State') ;
    }

    public function  images () {
        return  $this->hasMany('Eshopper\Image') ;
    }

    public function  brand() {
        return $this->belongsTo('Eshopper\Brand') ;
    }

    public function category() {
        return $this->belongsTo('Eshopper\Category') ;
    }
}
