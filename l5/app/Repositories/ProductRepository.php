<?php

namespace  Eshopper\Repositories;



use Eshopper\Category;
use Eshopper\Exceptions\ProductNotFoundException;
use Eshopper\Product;

class ProductRepository {

    protected  $product  ;
    public function  __construct( Product  $product ) {

        $this->product = $product ;
    }

    public function  getPaginatedProducts($limit) {

        return  $this->product->paginate($limit)->items()  ;
    }


    public  function getProduct($id)
    {
        return  $this->product->findOrFail($id) ;
    }
    public  function  getAllProductInfo($id) {

        $product = $this->product->with('availability', 'stock', 'state', 'prices', 'images')->find($id) ;

        if($product == null) throw new ProductNotFoundException($id ,  '') ;

        return $product ;
    }

    /**
     * @param $catId
     * @return
     * @internal param Category $category
     */
    public function getProductsOfCategory( $catId )
    {
        $category = Category::find($catId) ;

        // Get ids of descendants
        $categories = $category->descendants()->lists('id');

        $categories[] = $category->getKey();

        return   $this->product->whereIn('category_id', $categories)->get();
    }

}