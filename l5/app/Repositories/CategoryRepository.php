<?php

namespace  Eshopper\Repositories;
use Eshopper\Category;

class  CategoryRepository {
    protected  $category ;
    public function  __construct(Category $category ) {

        $this->category = $category ;
    }

    public function  getCategoriesAndSubCategories() {
        return $this->category->hasChildren()->get();
    }

}