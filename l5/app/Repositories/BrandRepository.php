<?php

namespace Eshopper\Repositories;

use Eshopper\Brand;

class  BrandRepository
{

    protected $brand;

    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    public function getBrandsProductsCount()
    {
        return $this->brand->with('products')->get();
    }

}