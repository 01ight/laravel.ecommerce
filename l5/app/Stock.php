<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'quantity',
        'product_id',
    ];

    public function products()
    {
        return $this->hasMany('Eshopper\Product');
    }
}
