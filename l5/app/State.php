<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = [
        'type',
        'product_id',
    ];

    public function  products() {
        return  $this->hasMany('Eshopper\Product') ;
    }

}
