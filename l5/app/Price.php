<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = [
        'type',
        'currency',
        'beginning_date' ,
        'ending_date',
        'product_id' ,
    ] ;

    public function  product() {
        return $this->belongsTo('Eshopper\Product') ;
    }
}