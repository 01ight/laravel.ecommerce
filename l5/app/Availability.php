<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    protected  $fillable  = [
        'type',
        'beginning_date' ,
        'ending_date'
    ] ;

    protected  $table = 'availabilities' ;

    public function products() {
        return  $this->hasMany('Eshopper\Product') ;
    }
}
