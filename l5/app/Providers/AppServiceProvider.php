<?php

namespace Eshopper\Providers;

use Eshopper\Repositories\BrandRepository;
use Eshopper\Repositories\CategoryRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param BrandRepository $brandRepository
     * @param CategoryRepository $categoryRepository
     */
    public function boot(BrandRepository $brandRepository,  CategoryRepository $categoryRepository)
    {
        view()->share('brands', $brandRepository->getBrandsProductsCount());

        view()->share('categories', $categoryRepository->getCategoriesAndSubCategories());


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
