<?php

namespace Eshopper;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = [
        'name',
        'description',
        'address',
        'email',
        'tel',
        'fax',
        'turnover'
    ];

    public function products() {
        return $this->hasMany('Eshopper\Product') ;
    }

}
