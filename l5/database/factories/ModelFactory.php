<?php

$factory->define(Eshopper\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

 function  getUsers()
{
    $users = \Eshopper\User::get(['id'])->toArray() ;
    $us = [];
    foreach ($users as $user )
        array_push($us, $user['id'])  ;

    return array_values($us) ;
}
$factory->define(Eshopper\Post::class, function (Faker\Generator $faker) {


    return [
        'title' => $faker->sentence(),
        'body' => $faker->text(300),
        'image_path' => '/images/blog/blog-one.jpg',
        'user_id'=> $faker->randomElement(getUsers())
    ];
});


$factory->define(Eshopper\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph(),
        'reference' => $faker->randomDigitNotNull,
        'rating' => $faker->numberBetween(0, 5),
        'details' => $faker->paragraphs(5, true) ,
        'availability_id' => $faker->numberBetween(1, 20),
        'state_id' => $faker->numberBetween(1, 20),
        'condition_id' => $faker->numberBetween(1, 20),
        'stock_id' => $faker->numberBetween(1, 20),
    ];
});
$factory->define(Eshopper\Price::class, function (Faker\Generator $faker) {
    return [
        'type' => $faker->randomElement(['sold', 'featured', 'promotion']),
        'value' => $faker->randomFloat(3, 2, 500),
        'beginning_date' => \Carbon\Carbon::now(),
        'ending_date' => $faker->dateTimeBetween('now', '2 years'),
        'product_id' => $faker->numberBetween(1, 20)
    ];
});

