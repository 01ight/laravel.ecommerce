<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name') ;
            $table->text('description') ;
            $table->string('reference') ;
            $table->integer('rating') ;
            $table->text('details') ;

            $table->integer('state_id')->unsigned() ;
            $table->integer('availability_id')->unsigned() ;
            $table->integer('condition_id')->unsigned() ;
            $table->integer('stock_id')->unsigned() ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
