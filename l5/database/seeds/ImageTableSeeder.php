<?php

use Illuminate\Database\Seeder;

class ImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $condition = new  \Eshopper\Image([
                'image_path' => '/images/shop/product'.$faker->numberBetween(1,12).'.jpg',
                'product_id' => $faker->numberBetween(1, 20),
            ]);

            $condition->save();

        }
    }
}
