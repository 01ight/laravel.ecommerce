<?php

use Eshopper\Post;
use Eshopper\Price;
use Eshopper\Product;
use Eshopper\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        factory(Product::class, 20)
            ->create()
            ->each(function($u) {
                $u->prices()->save(factory(Price::class)->make());
            });
         factory(User::class, 10)
            ->create()
            ->each(function($u) {
                $u->posts()->save(factory(Post::class)->make());
            });

        $this->call(AvailabilityTableSeeder1::class);
        $this->call(ConditionTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(StockTableSeeder::class);
        $this->call(ImageTableSeeder::class);

    }
}
