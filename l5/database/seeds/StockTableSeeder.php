<?php

use Illuminate\Database\Seeder;

class StockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $stock = new  \Eshopper\Stock([
                'quantity' => $faker->numberBetween(10, 50),
            ]);

            $stock->save();
        }
    }
}
