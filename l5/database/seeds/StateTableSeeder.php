<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i <20; $i++) {
            $condition = new  \Eshopper\State([
                'type' => $faker->randomElement(['Sale',  'featured', 'refurnished']),
            ]);

            $condition->save();
        }
    }
}
