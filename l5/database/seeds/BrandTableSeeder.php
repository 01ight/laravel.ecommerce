<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 7; $i++) {
            $brand = new \Eshopper\Brand([
                'name' => $faker->company,
                'description' => $faker->text(400),
                'address' => $faker->address,
                'email' => $faker->companyEmail,
                'tel' => $faker->phoneNumber,
                'fax' => $faker->phoneNumber,
                'turnover' => $faker->numberBetween(10, 1000)
            ]);
            $brand->save();
        }
    }
}
