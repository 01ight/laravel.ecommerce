<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create() ;
        $node = Eshopper\Category::create([


            'description' => $faker->text(300) ,
            'name'=>'SPORTSWEAR',
            'children' => [
                [
                    'name' => 'NIKE',
                    'description' => $faker->text(300) ,

                ],
                [
                    'name' => 'UNDER ARMOUR',
                    'description' => $faker->text(300) ,

                ],
                [
                    'name' => 'ADIDAS',
                    'description' => $faker->text(300) ,

                ],
                [
                    'name' => 'PUMA',
                    'description' => $faker->text(300) ,

                ],
                [
                    'name' => 'ASICS',
                    'description' => $faker->text(300) ,

                ],
            ],


        ]);
        $node->save() ;

        $node = Eshopper\Category::create([


            'description' => $faker->text(300) ,
            'name'=>'MENS',
            'children' => [
                [
                    'name' => 'NIKE',
                    'description' => $faker->text(300) ,

                ],
                [
                    'name' => 'UNDER ARMOUR',
                    'description' => $faker->text(300) ,

                ],
                [
                    'name' => 'ADIDAS',
                    'description' => $faker->text(300) ,

                ],
                [
                    'name' => 'PUMA',
                    'description' => $faker->text(300) ,

                ],
                [
                    'name' => 'ASICS',
                    'description' => $faker->text(300) ,

                ],
            ],


        ]);
        $node->save() ;
    }
}
