<?php

use Illuminate\Database\Seeder;

class ConditionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        for ($i = 0; $i <20; $i++) {
            $condition = new  \Eshopper\Condition([
                'value' => $faker->randomElement(['New',  'Refurbished', 'used']),
            ]);

            $condition->save();
        }
    }
}
