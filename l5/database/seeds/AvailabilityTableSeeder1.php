<?php

use Illuminate\Database\Seeder;

class AvailabilityTableSeeder1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        for ($i = 0; $i <20; $i++) {
            $availability = new  \Eshopper\Availability([
                'type' => $faker->randomElement(['In Stock',  'Arriving', 'Expired']),
                'beginning_date' => \Carbon\Carbon::now(),
                'ending_date' => $faker->dateTimeBetween('now' ,'5 days') ,
            ]);

            $availability->save();
        }
    }
}
