(function () {
     var app =  angular.module('Eshopper' , []) ;
    app.controller('Eshopper.CartCtrl' , function($scope) {

        this.name  = "Cart" ;
        this.cartProducts = [] ;
        this.getCartProducts = function () {

            return this.cartProducts ;
        }

        this.addProductToCart = function (product) {

            this.cartProducts.push(product) ;
        }

        this.getProductFromCart = function (product) {
            var index = this.cartProducts.indexOf(product);
            return this.cartProducts[index] ;

        }
        this.removeProductFromChart = function (product) {
            var index = this.cartProducts.indexOf(product);
            if (index > -1) {
             this.cartProducts.splice(index, 1);
            }
        }

        this.raiseProductQuantity= function(product, quantity) {
            var prod = this.getProductFromCart(product) ;
            pro.quantity = quantity ;

        }

    }) ;

})() ;